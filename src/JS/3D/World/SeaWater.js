import * as THREE from 'three'
import waterImg from '/media/textures/waternormals.jpg';
import {Water} from '../ShadersLib/Water';
import {OBJLoader} from '../Loaders/ObjLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import {useFrame, useLoader} from "@react-three/fiber";
import React from "react";

const textureLoader = new THREE.TextureLoader();

const SeaWater = () => {
    const myMesh = React.useRef();
    const clearTexture = textureLoader.load(water);
    const sun = new THREE.Vector3();
    const rot = Math.PI * -0.5;

    const waterGeometry = new THREE.PlaneGeometry( 1000, 1000 );

    const water = new Water(
        waterGeometry,
        {
            textureWidth: 512,
            textureHeight: 512,
            waterNormals: new THREE.TextureLoader().load( waterImg, function ( texture ) {

                texture.wrapS = texture.wrapT = THREE.RepeatWrapping;

            } ),
            sunDirection: new THREE.Vector3(),
            sunColor: 0xffffff,
            waterColor: '#000000',
            distortionScale: 1,
            fog: true,
            clipBias: 0,
            alpha: 0.4
        }
    );


    water.rotation.x = - Math.PI / 2;

let coords = {
    angle: 0,
    deep: false,
    side: false,
};
    useFrame(({ clock }) => {

        const a = coords.deep ? 0.001 :  - 0.001
        const b = coords.side ? 0.0005 :  - 0.0005
        myMesh.current.rotation.x += a;
        myMesh.current.rotation.z += b;
        if(myMesh.current.rotation.x > 0.05) coords.deep = false;
        else if(myMesh.current.rotation.x < -0.05) coords.deep = true;
        if(myMesh.current.rotation.z > 0.01) coords.side = false;
        else if(myMesh.current.rotation.z < -0.01) coords.side = true;


        water.material.uniforms[ 'time' ].value += 1.0 / 60.0;

    });
    const obj = useLoader(GLTFLoader, '/stock/yahch.gltf')
    const island = useLoader(GLTFLoader, '/stock/island.gltf')


    return (
        <>

            <primitive scale={[1,1,1]} object={water} position={[0,0,0]} />



            <group ref={myMesh} position={[0,0.1,0]} rotation={[0, 0, 0]}>
                <primitive scale={[0.2,0.2,0.2]} object={obj.scene} position={[0,0,0]} />
            </group>
            <group position={[-20,-1,0]} >
                <primitive scale={[10,10,10]} object={island.scene} />
            </group>
        </>

    )
}

export default SeaWater;