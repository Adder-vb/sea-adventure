import * as THREE from 'three'
import clearSky from '/media/textures/clearSky.png';

const textureLoader = new THREE.TextureLoader();

const SkySphere = () => {
    const clearTexture = textureLoader.load(clearSky);

    return (
        <mesh scale={[1,0.6, 1]}>
            <sphereGeometry args={[1000, 32, 32]}  />
            <meshStandardMaterial color={'lightblue'}
                                  side={THREE.BackSide}
                                  map ={clearTexture}
            />
        </mesh>
    )
}

export default SkySphere;