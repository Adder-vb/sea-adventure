import React, {Suspense} from "react";
import { Canvas, useFrame } from '@react-three/fiber'
import { OrbitControls } from '@react-three/drei'
import SkySphere from "./World/SkySphere";
import SeaWater from "./World/SeaWater";
export default function Scene() {
    return (
        <div className="scene">
            <Canvas camera={{ fov: 75, near: 0.1, far: 1000, position: [0, 2, 5], distance: [12] }}>
                <Suspense fallback={null}>
                    <ambientLight intensity={0.5} />
                    <pointLight color="#ffffff" position={[-250, 50, -250]} intensity={0.5}/>
                    <SkySphere position={[0, 0, 0]} />
                    <SeaWater/>
                    <OrbitControls {/*maxPolarAngle={Math.PI /2.5}
                                   minPolarAngle={Math.PI /4}
                                   minDistance={10}
                                   maxDistance={15}
                                   target={[5,2,3]}
                                   enablePan={false}*/}
                    />
                </Suspense>
            </Canvas>
        </div>

    )
}