import React from "react";
import ReactDOM from 'react-dom';
import App from './JS/App';
import './scss/style.scss';

// ========================================
ReactDOM.render(
        <App />,
    document.getElementById('app')
);

