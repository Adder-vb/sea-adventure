const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'none',
    entry: {
        app: path.join(__dirname, 'src', 'app.tsx')
    },
    output: {
        path: path.join(__dirname, '/app/'),
        publicPath: '/',
        filename: 'bundle-[name].[hash].js',
        chunkFilename: 'chunk-[name].[hash].js',
    },    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.tsx|js?$/,
                loader: 'ts-loader',
                options: {
                    happyPackMode: true // IMPORTANT! use happyPackMode mode to speed-up compilation and reduce errors reported to webpack
                },
                exclude: '/node_modules/'
            },

            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: { attrs: { class: 'css-webpack-asset' } }
                    },
                    {loader: 'css-loader'},
                    {loader: 'postcss-loader'},
                    {loader: 'sass-loader'}
                ]
            },
            {
                test: /\.(png|jpe?g|JPG|gif|svg|ttf|woff|woff2)$/,
                type: 'asset/resource',
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({template: './src/index.html', }),
    ],
    devServer: {
        historyApiFallback: true,
        contentBase: path.join(__dirname),
        compress: true,
        host: '0.0.0.0',
        public: 'localhost:3007',
        port: 3007,
        disableHostCheck: true,
    }
}
